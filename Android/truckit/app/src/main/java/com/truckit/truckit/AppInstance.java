package com.truckit.truckit;

import android.*;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.List;

import Models.TokenResponse;

/**
 * Created by Connor on 4/27/2017.
 */

public class AppInstance {

    private final static AppInstance instance = new AppInstance();

    // -- Server URL
    private static String serverUrl = "http://192.168.0.24:5000";

    // -- Endpoints
    private static String loginTokenEndpoint = "/Token";
    private static String registerAccountEndpoint = "/api/Account/Register";

    // -- User Account information.
    private TokenResponse appTokenResponse;

    // -- Context
    private Context mContext;

    // -- AppInstance contructor.
    public AppInstance() {
        appTokenResponse = null;
        mContext = null;
    }

    public static AppInstance GetAppInstance() { return instance; }

    public void init(Context context ) {
        if(mContext == null) {
            mContext = context;
        }
    }

    public static String getServerUrl() {
        return serverUrl;
    }

    // -- Endpoint Getters
    public static String getLoginTokenEndpoint() {
        return loginTokenEndpoint;
    }

    public static String getRegisterAccountEndpoint() {
        return registerAccountEndpoint;
    }

    // -- Functions relating to grabbing auth repsonse.
    public TokenResponse getAppAuthResponse() {
        return appTokenResponse;
    }

    public void setAppAuthResponse(TokenResponse appToken) {
        this.appTokenResponse = appToken;
    }

    public Context getContext() {
        return mContext;
    }

    // -- Location Services
    public Location getLastKnownLocation(Activity activity) {

        Location bestLocation = null;

        if(ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    PackageManager.PERMISSION_GRANTED);
        }
        else {
            LocationManager mLocationManager = (LocationManager)  mContext.getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);

            for (String provider : providers) {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
        }
        return bestLocation;
    }
}
