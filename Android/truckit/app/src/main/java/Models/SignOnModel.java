package Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Connor on 4/27/2017.
 */

public class SignOnModel {

    private String SignOnEmail;
    private String SignOnPassword;

    public SignOnModel(String email, String password) {
        this.SignOnEmail = email;
        this.SignOnPassword = password;
    }

    public String getSignOnEmail() {
        return SignOnEmail;
    }

    public String getSignOnPassword() {
        return SignOnPassword;
    }

    public String toJSON() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("email", getSignOnEmail());
            obj.put("password", getSignOnPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }
}
