package Models;

import org.json.JSONObject;

/**
 * Created by Connor on 4/28/2017.
 */

public class AuthMessage {
    private int code;
    private String message;

    public AuthMessage(int cde, String msg) {
        this.code = cde;
        this.message = msg;
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
