package Models;

import java.util.Iterator;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Connor on 5/5/2017.
 */

public class MapMarker {
    private UUID markerId;
    private UUID ownerId;
    private double latitude;
    private double longitude;

    public MapMarker() {}

    public MapMarker(String json) {
        fromJSONtoObj(json);
    }

    public UUID getMarkerId() {
        return markerId;
    }

    public void setMarkerId(UUID markerId) {
        this.markerId = markerId;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("marker_id", getMarkerId());
            jsonObject.put("owner_id", getOwnerId());
            jsonObject.put("latitude", getLatitude());
            jsonObject.put("longitude", getLongitude());
        } catch (JSONException e) {
            // TODO: Remove print stack trace.
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public void fromJSONtoObj(String json) {
        try{
            JSONObject obj = new JSONObject(json);
            for(Iterator<String> iter = obj.keys(); iter.hasNext();) {
                String key = iter.next();
                if(key.compareTo("marker_id") == 0) {
                    setMarkerId(UUID.fromString(obj.get(key).toString()));
                } else if(key.compareTo("owner_id") == 0) {
                    setOwnerId(UUID.fromString(obj.get(key).toString()));
                } else if(key.compareTo("latitude") == 0) {
                    setLatitude(Double.parseDouble(obj.get(key).toString()));
                } else if(key.compareTo("longitude") == 0) {
                    setLongitude(Double.parseDouble(obj.get(key).toString()));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
