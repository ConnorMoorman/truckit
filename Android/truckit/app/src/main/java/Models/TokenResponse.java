package Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by Connor on 4/29/2017.
 */

public class TokenResponse {

    private String AccessToken;

    private String TokenType;

    private String ExpiresIn;

    private String UserName;

    private String IssuedAt;

    private String ExpiresAt;

    public TokenResponse () {

    }

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getTokenType() {
        return TokenType;
    }

    public void setTokenType(String tokenType) {
        TokenType = tokenType;
    }

    public String getExpiresIn() {
        return ExpiresIn;
    }

    public void setExpiresIn(String exiresIn) {
        ExpiresIn = exiresIn;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getIssuedAt() {
        return IssuedAt;
    }

    public void setIssuedAt(String issuedAt) {
        IssuedAt = issuedAt;
    }

    public String getExpiresAt() {
        return ExpiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        ExpiresAt = expiresAt;
    }

    public String toJSON() {
        JSONObject obj = new JSONObject();
        try{
            obj.put("access_token", getAccessToken());
            obj.put("token_type", getTokenType());
            obj.put("expires_in", getExpiresIn());
            obj.put("userName", getUserName());
            obj.put(".issued", getIssuedAt());
            obj.put(".expires", getExpiresAt());
        } catch (JSONException e) {
            // TODO: Remove printing stacktrace
            e.printStackTrace();
        }
        return obj.toString();
    }

    public TokenResponse fromJSONtoObj(String json) {
        TokenResponse response = new TokenResponse();
        try{
            JSONObject obj = new JSONObject(json);
            for(Iterator<String> iter = obj.keys(); iter.hasNext();) {
                String key = iter.next();
                if(key.compareTo("access_token") == 0) {
                    response.setAccessToken(obj.get(key).toString());
                } else if(key.compareTo("token_type") == 0) {
                    response.setTokenType(obj.get(key).toString());
                } else if(key.compareTo("expires_in") == 0) {
                    response.setExpiresIn(obj.get(key).toString());
                } else if(key.compareTo("userName") == 0) {
                    response.setUserName(obj.get(key).toString());
                } else if(key.compareTo(".issued") == 0) {
                    String formatted = "";
                    // -- Need to convert date to something we care about.
                    if(obj.get(key).toString().contains(",")) {
                        formatted = obj.get(key).toString().replaceAll(",", "");
                    }
                    response.setIssuedAt(formatted);
                } else if(key.compareTo(".expires") == 0) {
                    String formatted = "";
                    // -- Need to convert date to something we care about.
                    if(obj.get(key).toString().contains(",")) {
                        formatted = obj.get(key).toString().replaceAll(",", "");
                    }
                    response.setExpiresAt(formatted);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public Boolean validExpirationDate() {

        Date expireDate = new Date(getExpiresAt());
        Date todaysDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");

        try{
            String tmpDate = dateFormat.format(expireDate);
            expireDate = dateFormat.parse(tmpDate);
        } catch (ParseException e) {
            // TODO: Remove print stack trace
            e.printStackTrace();
        }

        // -- Check if todays date is before the expiratin date (expired);
        if(todaysDate.before(expireDate)) {
            return true;
        }

        return false;
    }
}
