﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using TruckIt.DatabaseModels;
using TruckIt.Models;

namespace TruckIt.Migrations
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("truckitConnection", throwIfV1Schema: false) //DefaultConnection
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TruckMarker>().Property(m => m.longitude).HasPrecision(9, 6);
            modelBuilder.Entity<TruckMarker>().Property(m => m.latitude).HasPrecision(9, 6);
            modelBuilder.Entity<TruckStop>().Property(s => s.price).HasPrecision(6, 2);
            base.OnModelCreating(modelBuilder);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<TruckStop> TruckStops { get; set; }

        public DbSet<TruckMarker> TruckMarkers { get; set; }

        public DbSet<ProfileImages> ProfileImages { get; set; }

    }
}