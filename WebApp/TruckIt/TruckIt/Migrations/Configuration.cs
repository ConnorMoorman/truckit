namespace TruckIt.Migrations
{
    using DatabaseModels;
    using System;
    using System.CodeDom.Compiler;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Design;
    using System.Data.Entity.Migrations.Model;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.


            TruckMarker tm = new TruckMarker
            {
                marker_id = new Guid("d970728b-0d47-4343-b21c-277b8eb124cd"),
                longitude = 39.615822M,
                latitude = -84.2191148M
            };
            context.TruckMarkers.AddOrUpdate(m => m.marker_id, tm);

            TruckStop ts = new TruckStop
            {
                stop_id = new Guid("f8c8b175-03e9-4d27-9f7e-24d691521121"),
                owner_id = new Guid("ef5f2a85-2dfa-448e-8734-c14e335febd0"),
                truck_marker = tm,
                state = "OH",
                zip = "45865",
                address = "9221 Towering Pine Dr",
                time_pickup = DateTime.Now,
                time_leave = DateTime.Now.AddHours(2),
                date_created = DateTime.Now,
                description = "We're leaving then. $50.",
                price = 50.00M,
                in_progress = false,
            };            
            context.TruckStops.AddOrUpdate(t => t.stop_id, ts);

            context.SaveChanges();
        }
    }
}
