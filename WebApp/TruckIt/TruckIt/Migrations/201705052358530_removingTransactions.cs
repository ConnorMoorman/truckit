namespace TruckIt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removingTransactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TruckStops", "in_progress", c => c.Boolean(nullable: false));
            AddColumn("dbo.TruckStops", "rider_id", c => c.Guid());
            AddColumn("dbo.TruckStops", "date_completed", c => c.DateTime(precision: 0));
            CreateIndex("dbo.TruckStops", "stop_id");
            AddForeignKey("dbo.TruckStops", "stop_id", "dbo.TruckMarkers", "marker_id");
            DropColumn("dbo.TruckMarkers", "owner_id");
            DropColumn("dbo.TruckStops", "marker_id");
            DropColumn("dbo.TruckStops", "completed");
            DropColumn("dbo.TruckStops", "completed_trans_id");
            DropTable("dbo.TransactionActives");
            DropTable("dbo.TransactionsCompleteds");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TransactionsCompleteds",
                c => new
                    {
                        transId = c.Guid(nullable: false),
                        stopId = c.Guid(nullable: false),
                        ownerId = c.Guid(nullable: false),
                        riderId = c.Guid(nullable: false),
                        dateCompleted = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.transId);
            
            CreateTable(
                "dbo.TransactionActives",
                c => new
                    {
                        transId = c.Guid(nullable: false),
                        stopId = c.Guid(nullable: false),
                        ownerId = c.Guid(nullable: false),
                        riderId = c.Guid(nullable: false),
                        dateActive = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.transId);
            
            AddColumn("dbo.TruckStops", "completed_trans_id", c => c.Guid());
            AddColumn("dbo.TruckStops", "completed", c => c.Boolean(nullable: false));
            AddColumn("dbo.TruckStops", "marker_id", c => c.Guid(nullable: false));
            AddColumn("dbo.TruckMarkers", "owner_id", c => c.Guid(nullable: false));
            DropForeignKey("dbo.TruckStops", "stop_id", "dbo.TruckMarkers");
            DropIndex("dbo.TruckStops", new[] { "stop_id" });
            DropColumn("dbo.TruckStops", "date_completed");
            DropColumn("dbo.TruckStops", "rider_id");
            DropColumn("dbo.TruckStops", "in_progress");
        }
    }
}
