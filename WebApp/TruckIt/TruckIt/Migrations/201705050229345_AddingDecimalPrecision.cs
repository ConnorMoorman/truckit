namespace TruckIt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingDecimalPrecision : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TruckMarkers", "longitude", c => c.Decimal(nullable: false, precision: 9, scale: 6));
            AlterColumn("dbo.TruckMarkers", "latitude", c => c.Decimal(nullable: false, precision: 9, scale: 6));
            AlterColumn("dbo.TruckStops", "price", c => c.Decimal(nullable: false, precision: 6, scale: 2));
            AlterColumn("dbo.TruckStops", "completed_trans_id", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TruckStops", "completed_trans_id", c => c.String(unicode: false));
            AlterColumn("dbo.TruckStops", "price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.TruckMarkers", "latitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.TruckMarkers", "longitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
