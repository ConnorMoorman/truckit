namespace TruckIt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TruckMarkers", "longitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.TruckMarkers", "latitude", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.TruckStops", "price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TruckStops", "price", c => c.Decimal(nullable: false, precision: 15, scale: 10));
            AlterColumn("dbo.TruckMarkers", "latitude", c => c.Decimal(nullable: false, precision: 15, scale: 10));
            AlterColumn("dbo.TruckMarkers", "longitude", c => c.Decimal(nullable: false, precision: 15, scale: 10));
        }
    }
}
