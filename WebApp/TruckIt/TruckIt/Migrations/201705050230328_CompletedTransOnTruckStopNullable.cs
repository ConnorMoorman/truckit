namespace TruckIt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompletedTransOnTruckStopNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TruckStops", "completed_trans_id", c => c.Guid());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TruckStops", "completed_trans_id", c => c.Guid(nullable: false));
        }
    }
}
