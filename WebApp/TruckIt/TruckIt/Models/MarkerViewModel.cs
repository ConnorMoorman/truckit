﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckIt.Models
{
    public class MarkerViewModel
    {
        [Required]
        [Display(Name = "Latitude")]
        public double Latitude { get; set; }

        [Required]
        [Display(Name = "Longitude")]
        public double Longitude { get; set; }
        
        [Display(Name = "Distance")]
        public int? Distnace { get; set; }
    }
}