﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckIt.DatabaseModels
{
    public class ProfileImages
    {
        [Key]
        public Guid imageId { get; set; }

        public Guid ownerId { get; set; }

        public byte[] image { get; set; }
    }
}