﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TruckIt.DatabaseModels
{
    public class TruckMarker
    {
        [Key]
        public Guid marker_id { get; set; }

        [Required]
        public decimal latitude { get; set; }

        [Required]
        public decimal longitude { get; set; }
        
        // -- Relationship setup.
        public virtual TruckStop TruckStop { get; set; }
    }
}