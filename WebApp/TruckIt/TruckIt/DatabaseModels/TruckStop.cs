﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TruckIt.DatabaseModels
{
    public class TruckStop
    {
        [Key]
        public Guid stop_id { get; set; }

        [Required]
        public Guid owner_id { get; set; }

        [Required]
        public TruckMarker truck_marker { get; set; }

        public string state { get; set; }

        public string zip { get; set; }

        public string address { get; set; }

        [Required]
        public DateTime time_pickup { get; set; }

        [Required]
        public DateTime time_leave { get; set; }

        [Required]
        public DateTime date_created { get; set; }

        public string description { get; set; }

        [Required]
        public decimal price { get; set; }

        public bool in_progress { get; set; }
        
        public Guid? rider_id { get; set; }

        public DateTime? date_completed { get; set; }
    }
}