﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TruckIt.DatabaseModels;
using TruckIt.Migrations;

namespace TruckIt.Controllers
{
    [Authorize]
    public class TruckStopsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TruckStops
        public IQueryable<TruckStop> GetTruckStops()
        {
            return db.TruckStops;
        }

        // GET: api/TruckStops/5
        [ResponseType(typeof(TruckStop))]
        public IHttpActionResult GetTruckStop(Guid id)
        {
            TruckStop truckStop = db.TruckStops.Find(id);
            if (truckStop == null)
            {
                return NotFound();
            }

            return Ok(truckStop);
        }

        //// PUT: api/TruckStops/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutTruckStop(Guid id, TruckStop truckStop)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != truckStop.stop_id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(truckStop).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TruckStopExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/TruckStops
        [ResponseType(typeof(TruckStop))]
        public IHttpActionResult PostTruckStop(TruckStop truckStop)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TruckStops.Add(truckStop);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TruckStopExists(truckStop.stop_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = truckStop.stop_id }, truckStop);
        }

        //// DELETE: api/TruckStops/5
        //[ResponseType(typeof(TruckStop))]
        //public IHttpActionResult DeleteTruckStop(Guid id)
        //{
        //    TruckStop truckStop = db.TruckStops.Find(id);
        //    if (truckStop == null)
        //    {
        //        return NotFound();
        //    }

        //    db.TruckStops.Remove(truckStop);
        //    db.SaveChanges();

        //    return Ok(truckStop);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TruckStopExists(Guid id)
        {
            return db.TruckStops.Count(e => e.stop_id == id) > 0;
        }
    }
}