﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TruckIt.DatabaseModels;
using TruckIt.Migrations;
using TruckIt.Models;
using System.Device.Location;

namespace TruckIt.Controllers
{
    [Authorize]
    public class TruckMarkersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TruckMarkers
        public IQueryable<TruckMarker> GetTruckMarkers()
        {
            return db.TruckMarkers;
        }

        // POST: api/TruckMarkers
        [HttpPost]
        [ResponseType(typeof(List<TruckMarker>))]
        public async Task<List<TruckMarker>> GetTruckMarkers(MarkerViewModel markerVM)
        {
            List<TruckMarker> truckMarkers = new List<TruckMarker>();

            if (!ModelState.IsValid)
            {
                return truckMarkers;
            }

            if(markerVM.Distnace != null)
            {
                List<TruckMarker> dbListTruckMarkers = await db.TruckMarkers.ToListAsync();
                List<TruckMarker> returnTruckMarkers = new List<TruckMarker>();

                var userCoords = new GeoCoordinate(markerVM.Latitude, markerVM.Longitude);

                foreach (TruckMarker marker in dbListTruckMarkers)
                {
                    var dbCoords = new GeoCoordinate(markerVM.Latitude, markerVM.Longitude);

                    var distanceTo = userCoords.GetDistanceTo(dbCoords);

                    if(distanceTo <= markerVM.Distnace)
                    {
                        returnTruckMarkers.Add(marker);
                    }
                }

                return returnTruckMarkers;
            }
            else
            {
                truckMarkers = await db.TruckMarkers.ToListAsync();

                return truckMarkers;
            }
        }

        // GET: api/TruckMarkers/5
        //[ResponseType(typeof(TruckMarker))]
        //public IHttpActionResult GetTruckMarker(Guid id)
        //{
        //    TruckMarker truckMarker = db.TruckMarkers.Find(id);
        //    if (truckMarker == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(truckMarker);
        //}

        // PUT: api/TruckMarkers/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutTruckMarker(Guid id, TruckMarker truckMarker)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != truckMarker.marker_id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(truckMarker).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TruckMarkerExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/TruckMarkers
        //[ResponseType(typeof(TruckMarker))]
        //public IHttpActionResult PostTruckMarker(TruckMarker truckMarker)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.TruckMarkers.Add(truckMarker);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (TruckMarkerExists(truckMarker.marker_id))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtRoute("DefaultApi", new { id = truckMarker.marker_id }, truckMarker);
        //}

        // DELETE: api/TruckMarkers/5
        //[ResponseType(typeof(TruckMarker))]
        //public IHttpActionResult DeleteTruckMarker(Guid id)
        //{
        //    TruckMarker truckMarker = db.TruckMarkers.Find(id);
        //    if (truckMarker == null)
        //    {
        //        return NotFound();
        //    }

        //    db.TruckMarkers.Remove(truckMarker);
        //    db.SaveChanges();

        //    return Ok(truckMarker);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TruckMarkerExists(Guid id)
        {
            return db.TruckMarkers.Count(e => e.marker_id == id) > 0;
        }
    }
}